FROM python:3.8

WORKDIR /project

COPY ./requirements.txt /project/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /project/requirements.txt

COPY ./db /project

COPY ./temp /project

COPY ./testing /project

COPY ./add_data.py /project

COPY ./main.py /project

CMD ["python3", "add_data.py"]

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
