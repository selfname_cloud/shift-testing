This project was implemented to participate in the competition for courses from SHIFT. 
As part of the project, a system has been implemented to add a user to the database, authentication, by which you can get a secret token to 
view salary data and the date of promotion.

**To start the project, you need to perform the following actions:**

**1)install and activate a _virtual environment_ using virtualenv or poetry**

`python -m venv venv`

`%project%/venv/Scripts/activate.bat`

**2)first you need to install dependencies from _requirements.txt_ or of the _pyproject.toml_**

`pip install -r requirements.txt`

`poetry install`
   
**3)next, you need to add data to the database to do this, you need to _run add_data.py_**

`python add_data.py`

**(Attention! when adding data, a database error may appear related to unique values in the domain. 
To do this, you need to delete the app.db and run add_data.py again)**

**4)start project**

`uvicorn main:app --reload`

**For interaction it is necessary:**

**1)switch to http://127.0.0.1/docs**


**2)by _url: /api/user_ we _get a secret token_ if the user exists in the database.**

**(Attention! Since the data is generated automatically, 
it is possible to take the user for verification from the python command line, which are output when data is added to the database)**

**3)by _url: /api/token_ we _get data on salary and date raising_, if the token is valid.**

**(Attention! The token is valid for three minutes after which you need to get a new token)**

